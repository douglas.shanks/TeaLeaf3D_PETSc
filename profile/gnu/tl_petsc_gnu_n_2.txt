
   Tea Version    1.100
       MPI Version
   Task Count    128


   Tea Version    1.100
       MPI Version
   Task Count    128

 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.3133710E-13
Iteration count      111
      Solve Time    1.5594720840    Its    111     Time Per It    0.0140492981
 Wall clock    1.5613081455230713     
 Average time per cell    2.4980930328369139E-005
 Step time per cell       2.4975971221923828E-005
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.3255066E-13
Iteration count      110
      Solve Time    1.6047539711    Its    110     Time Per It    0.0145886725
 Wall clock    3.1669380664825439     
 Average time per cell    2.5335504531860352E-005
 Step time per cell       2.5683631896972655E-005
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.4147805E-13
Iteration count      109
      Solve Time    1.5218188763    Its    109     Time Per It    0.0139616411
 Wall clock    4.6896231174468994     
 Average time per cell    2.5011323293050132E-005
 Step time per cell       2.4356559753417969E-005
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3874190E-13
Iteration count      109
      Solve Time    1.5233840942    Its    109     Time Per It    0.0139760009
 Wall clock    6.2137899398803711     
 Average time per cell    2.4855159759521484E-005
 Step time per cell       2.4380306243896484E-005
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.3800033E-13
Iteration count      109
      Solve Time    1.5159749985    Its    109     Time Per It    0.0139080275
 Wall clock    7.7306139469146729     
 Average time per cell    2.4737964630126953E-005
 Step time per cell       2.4262718200683593E-005
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.3790958E-13
Iteration count      109
      Solve Time    1.6202499866    Its    109     Time Per It    0.0148646788
 Wall clock    9.3517100811004639     
 Average time per cell    2.4937893549601236E-005
 Step time per cell       2.5931102752685547E-005
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.3796110E-13
Iteration count      109
      Solve Time    1.5178890228    Its    109     Time Per It    0.0139255874
 Wall clock    10.870451927185059     
 Average time per cell    2.4846747262137275E-005
 Step time per cell       2.4293407440185546E-005
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.3801986E-13
Iteration count      109
      Solve Time    1.5195889473    Its    109     Time Per It    0.0139411830
 Wall clock    12.390887975692749     
 Average time per cell    2.4781775951385498E-005
 Step time per cell       2.4320526123046873E-005
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.3804272E-13
Iteration count      109
      Solve Time    1.6249971390    Its    109     Time Per It    0.0149082306
 Wall clock    14.016736984252930     
 Average time per cell    2.4918643527560763E-005
 Step time per cell       2.6007102966308592E-005
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.3801293E-13
Iteration count      109
      Solve Time    1.7198231220    Its    109     Time Per It    0.0157781938
 Wall clock    15.738556146621704     
 Average time per cell    2.5181689834594726E-005
 Step time per cell       2.7542640686035158E-005
Test problem   3 is within   0.4973799E-09% of the expected solution
 This test is considered PASSED
 Wall clock    15.740499019622803     
 First step overhead  -4.4227838516235352E-002

Profiler Output                 Time            Percentage
Timestep              :          0.0070          0.0446
Halo Exchange         :          0.0148          0.0938
Summary               :          0.0030          0.0193
Visit                 :          0.0000          0.0000
Tea Init              :          0.0621          0.3946
Tea Solve             :         15.6413         99.3700
Tea Reset             :         15.7241         99.8961
Set Field             :          0.0006          0.0035
Total                 :         31.4530        199.8219
The Rest              :        -15.7125        -99.8219
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./tea_leaf_gnu on a  named cn10 with 128 processors, by jdshanks Wed Apr 22 16:08:02 2020
Using Petsc Release Version 3.13.0, Mar 29, 2020 

                         Max       Max/Min     Avg       Total 
Time (sec):           1.595e+01     1.000   1.595e+01
Objects:              3.600e+01     1.000   3.600e+01
Flop:                 5.331e+09     1.070   5.117e+09  6.550e+11
Flop/sec:             3.343e+08     1.070   3.209e+08  4.107e+10
MPI Messages:         6.642e+03     2.000   5.258e+03  6.731e+05
MPI Message Lengths:  1.413e+08     2.081   2.132e+04  1.435e+10
MPI Reductions:       3.369e+03     1.000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flop
                            and VecAXPY() for complex vectors of length N --> 8N flop

Summary of Stages:   ----- Time ------  ----- Flop ------  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total    Count   %Total     Avg         %Total    Count   %Total 
 0:      Main Stage: 1.5946e+01 100.0%  6.5499e+11 100.0%  6.731e+05 100.0%  2.132e+04      100.0%  3.359e+03  99.7% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flop: Max - maximum over all processors
                  Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   AvgLen: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flop in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flop over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flop                              --- Global ---  --- Stage ----  Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   AvgLen  Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

BuildTwoSided         13 1.0 6.3997e-02 5.5 0.00e+00 0.0 1.2e+03 4.0e+00 1.3e+01  0  0  0  0  0   0  0  0  0  0     0
BuildTwoSidedF        11 1.0 6.0680e-02 5.5 0.00e+00 0.0 0.0e+00 0.0e+00 1.1e+01  0  0  0  0  0   0  0  0  0  0     0
DMCreateMat            1 1.0 1.5703e-01 1.0 0.00e+00 0.0 1.2e+03 5.3e+03 8.0e+00  1  0  0  0  0   1  0  0  0  0     0
SFSetGraph             2 1.0 3.5413e-04 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFSetUp                2 1.0 1.0964e-02 2.4 0.00e+00 0.0 2.4e+03 5.3e+03 2.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFBcastOpBegin      1103 1.0 2.1552e-01 2.1 0.00e+00 0.0 6.7e+05 2.1e+04 0.0e+00  1  0100100  0   1  0100100  0     0
SFBcastOpEnd        1103 1.0 9.5469e-01 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  5  0  0  0  0   5  0  0  0  0     0
SFPack              1103 1.0 1.7297e-01 2.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
SFUnpack            1103 1.0 4.0098e-04 1.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecTDot             2206 1.0 2.1727e+00 1.5 5.60e+08 1.1 0.0e+00 0.0e+00 2.2e+03 11 11  0  0 65  11 11  0  0 66 31729
VecNorm             1113 1.0 1.7062e+00 1.8 2.83e+08 1.1 0.0e+00 0.0e+00 1.1e+03  9  5  0  0 33   9  5  0  0 33 20386
VecCopy               20 1.0 1.0302e-02 1.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              1144 1.0 1.1531e-01 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
VecAXPY             2206 1.0 1.5954e+00 1.1 5.60e+08 1.1 0.0e+00 0.0e+00 0.0e+00  9 11  0  0  0   9 11  0  0  0 43211
VecAYPX             1093 1.0 6.1070e-01 1.3 2.78e+08 1.1 0.0e+00 0.0e+00 0.0e+00  3  5  0  0  0   3  5  0  0  0 55930
VecScatterBegin     1103 1.0 2.1958e-01 2.1 0.00e+00 0.0 6.7e+05 2.1e+04 0.0e+00  1  0100100  0   1  0100100  0     0
VecScatterEnd       1103 1.0 9.5747e-01 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  5  0  0  0  0   5  0  0  0  0     0
MatMult             1103 1.0 4.9880e+00 1.2 1.82e+09 1.1 6.7e+05 2.1e+04 0.0e+00 30 34100100  0  30 34100100  0 44751
MatSolve            1113 1.0 5.2549e+00 1.1 1.80e+09 1.1 0.0e+00 0.0e+00 0.0e+00 31 34  0  0  0  31 34  0  0  0 42175
MatLUFactorNum        10 1.0 1.5853e-01 1.1 2.71e+07 1.1 0.0e+00 0.0e+00 0.0e+00  1  1  0  0  0   1  1  0  0  0 20994
MatILUFactorSym        1 1.0 1.1607e-02 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyBegin      11 1.0 6.1051e-02 5.4 0.00e+00 0.0 0.0e+00 0.0e+00 1.1e+01  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        11 1.0 1.9765e-02 1.0 0.00e+00 0.0 1.2e+03 5.3e+03 5.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetRowIJ            1 1.0 9.0500e-0651.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetOrdering         1 1.0 8.8427e-04 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatZeroEntries        10 1.0 8.1911e-03 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSetUp              20 1.0 3.0215e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve              10 1.0 1.5104e+01 1.0 5.33e+09 1.1 6.7e+05 2.1e+04 3.3e+03 95100100100 99  95100100100 99 43365
PCSetUp               20 1.0 1.6913e-01 1.1 2.71e+07 1.1 0.0e+00 0.0e+00 0.0e+00  1  1  0  0  0   1  1  0  0  0 19679
PCSetUpOnBlocks       10 1.0 1.6877e-01 1.1 2.71e+07 1.1 0.0e+00 0.0e+00 0.0e+00  1  1  0  0  0   1  1  0  0  0 19720
PCApply             1113 1.0 5.3794e+00 1.1 1.80e+09 1.1 0.0e+00 0.0e+00 0.0e+00 31 34  0  0  0  31 34  0  0  0 41199
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

    Distributed Mesh     1              1         5552     0.
           Index Set     7              7      2102456     0.
   IS L to G Mapping     1              1       541352     0.
   Star Forest Graph     4              4         4544     0.
     Discrete System     1              1          936     0.
         Vec Scatter     2              2         1632     0.
              Vector    11             11      5162472     0.
       Krylov Solver     2              2         2888     0.
              Matrix     4              4     28206596     0.
      Preconditioner     2              2         1928     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 9.9e-08
Average time for MPI_Barrier(): 8.124e-06
Average time for zero size MPI_Send(): 3.72419e-05
#PETSc Option Table entries:
-ksp_type cg
-log_view
-pc_type bjacobi
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: PETSC_ARCH=linux-gnu --prefix=../petsc_install/gnu --with-cc=/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpicc --with-cxx=/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpicxx --with-fc=/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpif90 --with-mpiexec=/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpirun -with-blas-lib=/opt/arm/armpl-20.0.0_ThunderX2CN99_SUSE-12_gcc_9.2.0_aarch64-linux/lib/libarmpl_lp64.a -with-lapack-lib=/opt/arm/armpl-20.0.0_ThunderX2CN99_SUSE-12_gcc_9.2.0_aarch64-linux/lib/libarmpl_lp64.a --with-debugging=0 COPTFLAGS="-O3 -march=native -mtune=native" CXXOPTFLAGS="-O3 -march=native -mtune=native" FOPTFLAGS="-O3 -march=native -mtune=native"
-----------------------------------------
Libraries compiled on 2020-04-21 07:09:34 on fulhame-login1 
Machine characteristics: Linux-4.12.14-150.22-default-aarch64-with-glibc2.17
Using PETSc directory: /scratch3/home/fh04/fh04/jdshanks/petsc_install/gnu
Using PETSc arch: 
-----------------------------------------

Using C compiler: /opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpicc  -fPIC -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fstack-protector -fvisibility=hidden -O3 -march=native -mtune=native  
Using Fortran compiler: /opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpif90  -fPIC -Wall -ffree-line-length-0 -Wno-unused-dummy-argument -O3 -march=native -mtune=native    
-----------------------------------------

Using include paths: -I/scratch3/home/fh04/fh04/jdshanks/petsc_install/gnu/include
-----------------------------------------

Using C linker: /opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpicc
Using Fortran linker: /opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/bin/mpif90
Using libraries: -Wl,-rpath,/scratch3/home/fh04/fh04/jdshanks/petsc_install/gnu/lib -L/scratch3/home/fh04/fh04/jdshanks/petsc_install/gnu/lib -lpetsc -Wl,-rpath,/opt/arm/armpl-20.0.0_ThunderX2CN99_SUSE-12_gcc_9.2.0_aarch64-linux/lib -L/opt/arm/armpl-20.0.0_ThunderX2CN99_SUSE-12_gcc_9.2.0_aarch64-linux/lib -Wl,-rpath,/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/lib -L/opt/arm/mpi/openmpi/gcc_9.2/inbox/4.0.2/lib -Wl,-rpath,/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib/gcc/aarch64-linux/9.2.0 -L/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib/gcc/aarch64-linux/9.2.0 -Wl,-rpath,/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib/gcc -L/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib/gcc -Wl,-rpath,/opt/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib64 -L/opt/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib64 -Wl,-rpath,/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib64 -L/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib64 -Wl,-rpath,/opt/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib -L/opt/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib -Wl,-rpath,/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/aarch64-linux/lib -L/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/aarch64-linux/lib -Wl,-rpath,/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib -L/scratch3/arm/gcc-9.2.0_Generic-AArch64_SUSE-12_aarch64-linux/lib -larmpl_lp64 -lm -lstdc++ -ldl -lmpi_usempif08 -lmpi_usempi_ignore_tkr -lmpi_mpifh -lmpi -lgfortran -lm -lgfortran -lm -lgcc_s -lpthread -lstdc++ -ldl
-----------------------------------------


#!/bin/bash

echo "Launching TeaLeaf3d PETSc with gnu compiler"

compiler="gnu"

for i in 1 2 4; #8 16;
do
	sbatch --nodes $i --output ./profile/${compiler}/tl_petsc_${compiler}_n_$i.txt tea_job_${compiler}.pbs
done
